import sys
import csv
import nltk
from nltk.tokenize import word_tokenize
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import PlaintextCorpusReader
from nltk.corpus import stopwords

stopset = list(set(stopwords.words('english')))

#nltk.download()

def word_features(text):
    words = word_tokenize(text)
    words = words[2:-1]
    words = nltk.pos_tag(words)
    return dict([(word, True) for word in words if word not in stopset])

sampleTweets = csv.reader(open('samples.csv', 'r'), delimiter=',', quotechar='|')

trainset = [(word_features(row[1]), row[0])
             for row in sampleTweets]

classifier = NaiveBayesClassifier.train(trainset)
classifier.show_most_informative_features()

print("[===========================================]")

tweetlist = PlaintextCorpusReader('data', '.*\.tweet')

with open('output.log', 'w+') as f:
  for tweet in tweetlist.fileids():
      category = classifier.classify(word_features(tweetlist.raw(tweet)))
      line = "%s [%s]: %s" % (tweet, category, tweetlist.raw(tweet).encode('utf8'))
      print(line)
      f.write(line + "\n")
