import sys
from twitter import processTweet
from twitter import getApi

api = getApi()

r = api.request("search/tweets", {
    'q': '#nerdblock',
    'count': 100,
    'lang': 'en'
})

for item in r:
  filename = 'data/' + str(item["id"]) + '.tweet'
  with open(filename, 'w+') as f:
    text = item["text"].encode('UTF-8')
    f.write(str(text))
