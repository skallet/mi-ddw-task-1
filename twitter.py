import re
from TwitterAPI import TwitterAPI

def getApi():
  return TwitterAPI(
    "***",
    "***",
    "***",
    "***"
  )

def processTweet(tweet):
    # process the tweets

    #Convert to lower case
    tweet = str(tweet).lower()
    #Convert www.* or https?://* to URL
    tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','URL',tweet)
    #Convert @username to AT_USER
    tweet = re.sub('@[^\s]+','AT_USER',tweet)
    #Remove additional white spaces
    tweet = re.sub('[\s]+', ' ', tweet)
    #Replace #word with word
    tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
    #trim
    tweet = tweet.strip('\'"')
    return tweet.encode('UTF-8')
