import sys
from twitter import processTweet
from twitter import getApi

api = getApi()

r = api.request("search/tweets", {
    'q': '#nerdblock',
    'count': 30,
    'lang': 'en'
})

with open('samples.csv', 'w+') as f:
  for item in r:
    text = item["text"].encode('UTF-8')
    tweet = processTweet(text)
    print("==============================")
    print(text)
    inputType = input("Type [r - review; x (default) - unboxing]:")
    tweetType = {
      'r': 'review',
      'x': 'unboxing'
    }.get(inputType, 'unboxing')
    line = "|" + str(tweetType) + "|,|" + str(tweet) + "|"
    f.write(line + "\n")
